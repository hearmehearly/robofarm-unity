﻿using UnityEngine;


public class GridTestAction : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var gridGenerator = FindObjectOfType<HexagonGridGenerator>();

            if (gridGenerator != null)
            {
                gridGenerator.RefreshGrid();
            }
        }
    }
}
