﻿/// Take from https://pastebin.com/gX5YhzBe
using System.Collections.Generic;
using UnityEngine;


public class HexagonGridGenerator : MonoBehaviour
{
    private const float StartHexWidth = 1.732f;
    private const float StartHexHeight = 2.0f;

    [SerializeField] private Transform hexPrefab = default;
    [SerializeField] private float gap = default;

    [SerializeField] private int gridWidth = 11;
    [SerializeField] private int gridHeight = 11;

    private float hexWidth;
    private float hexHeight;

    private Vector3 startPos;

    private List<Transform> gridElements = new List<Transform>();



    public void RefreshGrid()
    {
        RefreshGapValue();
        RefreshStartPos();
        DestroyGrid();
        CreateGrid();
    }
    

    private void RefreshGapValue()
    {
        hexWidth = StartHexWidth + StartHexWidth * gap;
        hexHeight = StartHexHeight + StartHexHeight * gap;
    }


    private void RefreshStartPos()
    {
        float offset = 0;
        if (gridHeight * 0.5f % 2 != 0)
            offset = hexWidth / 2;

        float x = -hexWidth * (gridWidth / 2) - offset;
        float z = hexHeight * 0.75f * (gridHeight / 2);

        startPos = new Vector3(x, 0, z);
    }


    private void CreateGrid()
    {
        for (int y = 0; y < gridHeight; y++)
        {
            for (int x = 0; x < gridWidth; x++)
            {
                Transform hex = Instantiate(hexPrefab, transform);
                Vector2 gridPos = new Vector2(x, y);
                hex.position = CalculateWorldPos(gridPos);
                hex.name = "Hexagon" + x + "|" + y;

                gridElements.Add(hex);
            }
        }


        Vector3 CalculateWorldPos(Vector2 gridPos)
        {
            float offset = 0;
            if (gridPos.y % 2 != 0)
                offset = hexWidth / 2;

            float x = startPos.x + gridPos.x * hexWidth + offset;
            float z = startPos.z - gridPos.y * hexHeight * 0.75f;

            return new Vector3(x, 0, z);
        }
    }


    private void DestroyGrid()
    {
        foreach (var e in gridElements)
        {
            Destroy(e.gameObject);
        }

        gridElements.Clear();
    }
}
